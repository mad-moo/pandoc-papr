# Getting started

This document was created from the files in the `/sources` directory. If you
want to see how exactly this document looks you can always open the files with
your favorite text editor and take a peek!

`paper.toml` controls how the document is created. All options have helpful
comments that tell you everything you need to know.

Once you are happy with your work you can create the document (in PDF format) by
running this command:

```
papr /path/to/paper.toml paper.pdf
```

# Welcome to papr!

papr is a wrapper that allows you to use [pandoc](https://www.pandoc.org) in a
very easy and organized fashion. Pandoc is an extremely powerful tool that can
convert between dozens of different document formats, many of which you probably
already know. [See the supported input and output formats
here.](https://pandoc.org/MANUAL.html#general-options)

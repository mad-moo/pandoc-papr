import subprocess
import argparse
import sys
from .errors import *
from typing import List
from pathlib import Path


def convert(in_paths: List[Path],
            out_path: Path,
            custom_args: List[str]):

    input_files = [ str(x.resolve()) for x in in_paths]
    output_file = [ '-o', str(out_path.resolve()) ]

    command =                      \
        ['pandoc'] +               \
        custom_args +              \
        input_files +              \
        output_file

    try:
        proc_result = subprocess.run(
            command,
            stdout=sys.stdout,
            stderr=sys.stderr,
            check=True
        )
    except subprocess.CalledProcessError as err:
        raise UserError(f'Pandoc returned unsucessfully') from err

